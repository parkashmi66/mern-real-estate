import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

export const sendCookie = (user, res, message, statusCode = 200, data = {}) => {
  const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);

  return res
    .status(statusCode)
    .cookie("access_token", token, { httpOnly: true, maxAge: 30 * 60 * 1000 })
    .json({ success: true, message, data: data });
};
