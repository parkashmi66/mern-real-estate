import express from "express";
import {
  createList,
  deleteList,
  getListById,
  getListings,
  updateListing,
} from "../controllers/listing.js";
import { verifyToken } from "../utils/verifyUser.js";

const router = express.Router();

router.post("/create", verifyToken, createList);
router.delete("/delete/:id", verifyToken, deleteList);
router.post("/update/:id", verifyToken, updateListing);
router.get("/get/:id", verifyToken, getListById);
router.get("/get", getListings);

export default router;
