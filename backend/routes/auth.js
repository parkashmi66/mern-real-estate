import express from "express";
import {
  googleSignIn,
  signin,
  signup,
  singOutUser,
} from "../controllers/auth.js";

const router = express.Router();

router.post("/signup", signup);
router.post("/signin", signin);
router.post("/google", googleSignIn);
router.get("/signout", singOutUser);

export default router;
