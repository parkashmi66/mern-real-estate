import express from "express";
import {
  deletUser,
  getListingByUserId,
  getUserById,
  updateUser,
} from "../controllers/user.js";
import { verifyToken } from "../utils/verifyUser.js";

const router = express.Router();

router.post("/update/:id", verifyToken, updateUser);
router.delete("/delete/:id", verifyToken, deletUser);
router.get("/listings/:id", verifyToken, getListingByUserId);
router.get("/:id", verifyToken, getUserById);

export default router;
