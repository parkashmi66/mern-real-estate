import User from "../models/user.js";
import bcrypt from "bcrypt";
import { sendCookie } from "../utils/cookie.js";
import { errorHandler } from "../utils/error.js";

export const signup = async (req, res, next) => {
  const { username, password, email } = req.body;

  try {
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(409).json({
        success: false,
        messgae: "User Exists already",
      });
    }
    const hashPassword = bcrypt.hashSync(password, 10);

    const user = new User({ username, password: hashPassword, email });
    await user.save();
    res.status(201).json({
      success: true,
      message: "User Created Successfully",
    });
  } catch (error) {
    next(error);
  }
};

export const signin = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    const validUser = await User.findOne({ email });
    if (!validUser) {
      return next(errorHandler(404, "User Not Found"));
    }
    const isMatch = bcrypt.compareSync(password, validUser.password);
    if (!isMatch) return next(errorHandler(401, "Invalid Crednetials"));

    const { password: pass, ...rest } = validUser._doc;

    sendCookie(validUser, res, `Welcome back ${validUser.username}`, 200, rest);
  } catch (error) {
    next(error);
  }
};

export const googleSignIn = async (req, res, next) => {
  const { email, username, photo } = req.body;
  try {
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      const { password: pass, ...rest } = existingUser._doc;
      sendCookie(
        existingUser,
        res,
        `Welcome back ${existingUser.username}`,
        200,
        rest
      );
    } else {
      const generatedPassword =
        Math.random().toString(36).slice(-8) +
        Math.random().toString(36).slice(-8);

      const hashPassword = bcrypt.hashSync(generatedPassword, 10);

      const newUser = new User({
        email,
        username: username.split(" ").join("").toLowerCase(),
        password: hashPassword,
        avatar: photo,
      });
      await newUser.save();
      const { password: pass, ...rest } = newUser._doc;
      sendCookie(newUser, res, `Welcome back ${newUser.username}`, 200, rest);
    }
  } catch (error) {
    next(error);
  }
};

export const singOutUser = async (req, res, next) => {
  try {
    res.clearCookie("access_token");
    res.status(200).json({
      success: true,
      message: "User has been logged Out!",
    });
  } catch (error) {
    next(error);
  }
};
