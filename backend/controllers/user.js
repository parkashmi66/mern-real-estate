import User from "../models/user.js";
import { errorHandler } from "../utils/error.js";
import Listing from "../models/listing.js";
import bcrypt from "bcrypt";

export const updateUser = async (req, res, next) => {
  try {
    if (req.user.id !== req.params._id) {
      return next(errorHandler(401, "You can only update your own account!"));
    }

    const updateObj = {};
    if (req.body.username) updateObj.username = req.body.username;
    if (req.body.email) updateObj.email = req.body.email;
    if (req.body.password) {
      updateObj.password = bcrypt.hashSync(req.body.password, 10);
    }
    if (req.body.avatar) updateObj.avatar = req.body.avatar;
    const updatedUser = await User.findByIdAndUpdate(req.params.id, updateObj, {
      new: true,
    });

    if (!updatedUser) {
      return next(errorHandler(404, "User not found"));
    }
    const { password, ...rest } = updatedUser._doc;

    res.status(200).json({
      success: true,
      data: rest,
      message: "User Updated Successfully !",
    });
  } catch (error) {
    next(error);
  }
};

export const deletUser = async (req, res, next) => {
  try {
    if (req.user.id === req.params._id) {
      return next(errorHandler(401, "You can only Delete your own account!"));
    }
    await User.findByIdAndDelete(req.params.id);
    res.clearCookie("access_token");
    res.status(200).json({
      success: true,
      message: "User Deleted Successfully",
    });
  } catch (error) {
    next(error);
  }
};

export const getListingByUserId = async (req, res, next) => {
  try {
    if (req.params.id !== req.user.id) {
      const listings = await Listing.find({ userRef: req.params.id });
      res.status(200).json({
        success: true,
        list: listings,
      });
    } else {
      return next(errorHandler(401, "You can only view own account Listing"));
    }
  } catch (error) {
    next(error);
  }
};

export const getUserById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    console.log("user is", user);
    if (!user) {
      return next(errorHandler(401, "User Not Found"));
    }

    const { password: pass, ...rest } = user._doc;
    res.status(200).json({
      success: true,
      message: "User Fetched",
      data: rest,
    });
  } catch (error) {
    next(error);
  }
};
