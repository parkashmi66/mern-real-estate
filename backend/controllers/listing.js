import Listing from "../models/listing.js";
import { errorHandler } from "../utils/error.js";


export const createList = async (req, res, next) => {
  try {
    const listing = await Listing.create(req.body);

    const { userRef, ...responseData } = listing.toObject();

    return res.status(201).json({
      success: true,
      message: "Listing Created",
      data: {
        _id: listing._id,
        ...responseData,
      },
    });
  } catch (error) {
    next(error);
  }
};
export const deleteList = async (req, res, next) => {
  const listing = await Listing.findById(req.params.id);

  if (!listing) {
    return next(errorHandler(404, "Listing Not Found"));
  }
  if (req.user._id !== listing.userRef.toString()) {
    return next(errorHandler(401, "You Can Delete only Own listings "));
  }
  try {
    await Listing.findByIdAndDelete(req.params.id);
    return res.status(200).json({
      success: true,
      message: `${listing.name} Deleted Successfully`,
    });
  } catch (error) {
    next(error);
  }
};

export const updateListing = async (req, res, next) => {
  const listing = await Listing.findById(req.params.id);

  if (!listing) {
    return next(errorHandler(404, "Listing Not Found"));
  }
  if (req.user._id !== listing.userRef.toString()) {
    return next(errorHandler(401, "You Can Update only Own listings "));
  }
  try {
    const updateList = await Listing.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true }
    );
    return res.status(200).json({
      success: true,
      data: updateList,
      message: "Listing Updated Successfully",
    });
  } catch (error) {
    next(error);
  }
};

export const getListById = async (req, res, next) => {
  try {
    const listing = await Listing.findById(req.params.id);

    if (!listing) {
      return next(errorHandler(404, "Listing Not Found"));
    }

    return res.status(200).json({
      success: true,
      data: listing,
    });
  } catch (error) {
    next(error);
  }
};

export const getListings = async (req, res, next) => {
  try {
    const limit = parseInt(req.query.limit) || 9;
    const startIndex = parseInt(req.query.startIndex) || 0;
    let offer = req.query.offer;
    if (offer === undefined || offer === "false") {
      offer = { $in: [false, true] };
    }
    let furnished = req.query.furnished;
    if (furnished === undefined || furnished === "false") {
      furnished = { $in: [false, true] };
    }
    let parking = req.query.parking;
    if (parking === undefined || parking === "false") {
      parking = { $in: [false, true] };
    }
    let type = req.query.type;
    if (type === undefined || type === "all") {
      type = { $in: ["sale", "rent"] };
    }
    const searchTerm = req.query.searchTerm || "";
    const sort = req.query.sort || "createdAt";
    const order = req.query.order || "desc";

    const listings = await Listing.find({
      $or: [
        { name: { $regex: searchTerm, $options: "i" } },
        { address: { $regex: searchTerm, $options: "i" } },
      ],
      offer,
      furnished,
      parking,
      type,
    })
      .sort({
        [sort]: order,
      })
      .limit(limit)
      .skip(startIndex);

    return res.status(200).json({
      success: true,
      data: listings,
    });
  } catch (error) {
    next(error);
  }
};
