import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import userRouter from "./routes/user.js";
import authRouter from "./routes/auth.js";
import useListRouter from "./routes/listing.js";
import cookieParser from "cookie-parser";
import path from "path"

dotenv.config();

const PORT = 5001;
const __dirname= path.resolve()
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => console.log("data base is connected"));

const app = express();
app.use(express.json());
app.use(cookieParser());

app.use("/api/users", userRouter);
app.use("/api/auth", authRouter);
app.use("/api/list", useListRouter);
app.use(express.static(path.join(__dirname,"/frontend/dist")));
app.get("*", (req, res)=>{
  res.sendFile(path.join(__dirname,"frontend", "dist", "index.html"))
})

app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  const message = err.message || "Internal Server Error!";
  res.status(statusCode).json({
    success: false,
    statusCode,
    message,
  });
});

app.listen(PORT, () => {
  console.log(`App is running on port ${PORT}`);
});
