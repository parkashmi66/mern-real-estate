// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDV1FWQaEGdRpGMOBXVfjetInzFIjSrJh4",
  authDomain: "estate-explore-auth.firebaseapp.com",
  projectId: "estate-explore-auth",
  storageBucket: "estate-explore-auth.appspot.com",
  messagingSenderId: "884616499778",
  appId: "1:884616499778:web:479be67c438d4fe693d3db",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
