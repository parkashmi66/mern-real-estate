import React from "react";
import { GoogleAuthProvider, getAuth, signInWithPopup } from "firebase/auth";
import { app } from "../../firebase";
import axios from "axios";
import { useDispatch } from "react-redux";
import { signInFailure, signInSuccess } from "../redux/user/userSlice";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
const OAuth = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleOnGoogleClick = async () => {
    try {
      const provider = new GoogleAuthProvider();
      const auth = getAuth(app);
      const result = await signInWithPopup(auth, provider);

      const res = await axios.post("/api/auth/google", {
        username: result?.user?.displayName,
        email: result?.user?.email,
        photo: result?.user?.photoURL,
      });
      if (res?.data?.success === false) {
        toast.error(res?.data?.message);
        dispatch(signInFailure());
        return;
      }
      dispatch(signInSuccess(res?.data?.data));
      navigate("/");
      if (res?.data?.success === true) {
        toast.success(res?.data?.message);
      }
    } catch (error) {
      // const errorMessage = error?.response?.data.message;
      // toast.error(errorMessage);
      dispatch(signInFailure());
    }
  };
  return (
    <button
      onClick={handleOnGoogleClick}
      type="button"
      className="bg-red-700 rounded-lg uppercase hover:opacity-95 text-white p-3"
    >
      Continue with Google
    </button>
  );
};

export default OAuth;
