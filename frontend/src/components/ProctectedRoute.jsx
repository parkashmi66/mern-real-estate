import React from "react";
import { Outlet, Navigate } from "react-router-dom";
import userLoggedIn from "../hooks/useAuthenticated";

const ProctectedRoute = () => {
  const isLoggedIn = userLoggedIn();
  return isLoggedIn ? <Outlet /> : <Navigate to="sign-in" />;
};

export default ProctectedRoute;
