import React from "react";
import { Link } from "react-router-dom";
import { MdLocationOn } from "react-icons/md";

export default function ListingCard({ listing }) {
  console.log("listings", listing);
  return (
    <div className="bg-white shadow-md hover:shadow-lg transition-shadow overflow-hidden rounded-lg w-full sm:w-[330px]">
      <Link to={`listing/${listing._id}`}>
        <img
          className="h-[320px] sm:h-[220px] object-cover w-full hover:scale-105 transition-scale duration-100 "
          src={
            listing?.imageUrls[0] ||
            "https://assets-global.website-files.com/619e763bb3de7b56e6107aeb/61f2b135991e87e97b5bfd4c_26-real-estate-questions-Hero-Image.png"
          }
          alt="listing-cover"
        />
      </Link>
      <div className="p-3 flex flex-col gap-2">
        <p className="text-lg font-semibold text-slate-700 truncate">
          {listing?.name}
        </p>
        <div className="flex gap-1 items-center">
          <MdLocationOn className="h-4 w-4 text-green" />
          <p className="text-sm text-gray-600 truncate w-full">
            {listing?.address}
          </p>
        </div>
        <p className="text-gray-600 text-sm line-clamp-2">
          {listing?.description}
        </p>
        <p className="text-slate-500 mt-2 font-semibold flex items-center">
          $
          {listing?.offer
            ? listing?.discountedPrice.toLocaleString("en-US")
            : listing?.price.toLocaleString("en-US")}
          {listing?.type === "rent" && "/month"}
        </p>
        <div className="text-slate-700">
          <div className="text-xs font-bold">
            {listing?.bedRooms > 1
              ? `${listing?.bedRooms} beds`
              : `${listing?.bedRooms} bed`}
          </div>
          <div className="text-xs font-bold">
            {listing?.bathRooms > 1
              ? `${listing?.bathRooms} bathrooms`
              : `${listing?.bathRooms} bathroom`}
          </div>
        </div>
      </div>
    </div>
  );
}
