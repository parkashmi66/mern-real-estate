import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userInfo: null,
  loading: false,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    signInStart: (state) => {
      state.loading = true;
    },
    signInSuccess: (state, action) => {
      state.userInfo = action.payload;
      state.loading = false;
    },
    signInFailure: (state, action) => {
      state.loading = false;
    },
    updateStart: (state) => {
      state.loading = true;
    },
    updateSuccess: (state, action) => {
      state.userInfo = action.payload;
      state.loading = false;
    },
    updateFailure: (state, action) => {
      state.loading = false;
    },
    userLogout: (state) => {
      localStorage.clear();
      (state.userInfo = null), (state.loading = false);
    },
    deleteUserStart: (state) => {
      state.loading = true;
    },
    deleteUserSuccess: (state) => {
      state.loading = false;
      state.userInfo = null;
      // localStorage.clear();
    },
    deleteUserFailure: (state) => {
      state.loading = false;
    },
    signOutStart: (state) => {
      state.loading = true;
    },
    signOutSuccess: (state) => {
      (state.loading = false), localStorage.clear();
      state.userInfo = null;
    },
    signOutFailure: (state) => {
      state.loading = false;
    },
  },
});

export const {
  signInFailure,
  signInStart,
  signInSuccess,
  updateStart,
  updateSuccess,
  updateFailure,
  userLogout,
  deleteUserStart,
  deleteUserFailure,
  deleteUserSuccess,
  signOutFailure,
  signOutSuccess,
  signOutStart,
} = userSlice.actions;

export default userSlice.reducer;
