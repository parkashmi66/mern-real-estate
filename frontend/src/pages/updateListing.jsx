import React, { useEffect, useState } from "react";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { app } from "../../firebase";
import toast from "react-hot-toast";
import axios from "axios";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

export default function UpdateListing() {
  const [listingData, setListingData] = useState({
    name: "",
    description: "",
    address: "",
    price: 1,
    discountedPrice: 50,
    bathRooms: 1,
    bedRooms: 1,
    furnished: false,
    parking: false,
    type: "",
    offer: false,
    imageUrls: [],
  });

  console.log("listin data", listingData);
  const [files, setFiles] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errorMessages, setErrorMessages] = useState({
    name: "",
    description: "",
    address: "",
  });
  const { userInfo } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const params = useParams();

  console.log("use Parmas", params.id);

  const handleInputChange = (event) => {
    const { id, type, checked, value } = event.target;
    const newValue = type === "checkbox" ? checked : value;

    if (type === "checkbox") {
      if (id === "rent" || id === "sale") {
        // For "rent" and "sale" checkboxes, set the corresponding type in the state
        setListingData({ ...listingData, type: id });
      } else {
        setListingData({ ...listingData, [id]: newValue });
      }
    } else {
      setListingData({ ...listingData, [id]: newValue });
    }
  };

  const checkTypes = [
    {
      id: "parking",
      label: "Parking Spot",
    },
    {
      id: "furnished",
      label: "Furnished",
    },
    {
      id: "offer",
      label: "Offer",
    },
  ];

  const handleImageSubmit = (e) => {
    if (files.length > 0 && files.length + listingData.imageUrls.length < 7) {
      const promises = [];
      for (let i = 0; i < files.length; i++) {
        promises.push(storeImage(files[i]));
      }
      setUploading(true);
      Promise.all(promises)
        .then((urls) => {
          setListingData({
            ...listingData,
            imageUrls: listingData.imageUrls.concat(urls),
          });
          // setImageUploadError(false);
          setUploading(false);
        })
        .catch((error) => {
          // setImageUploadError("");
          toast.error("Image Upload Failed (2mb max per image)");
          setUploading(false);
        });
    } else {
      toast.error("You can Upload 6 images per listing");
      setUploading(false);
    }
  };

  const storeImage = async (file) => {
    return new Promise((resolve, reject) => {
      const storage = getStorage(app);
      const fileName = new Date().getTime() + file.name;
      const storageRef = ref(storage, fileName);
      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("progress", progress);
        },
        (error) => {
          reject(error);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) =>
            resolve(downloadUrl)
          );
        }
      );
    });
  };

  const handleImageDelete = (url) => {
    const updatedImageUrls = listingData.imageUrls.filter(
      (image) => image !== url
    );
    setListingData({
      ...listingData,
      imageUrls: updatedImageUrls,
    });
  };

  const handleCreateListSubmit = async (e) => {
    e.preventDefault();
    // Check for missing required fields

    const missingFields = {};
    if (!listingData.name) {
      missingFields.name = "Name is required.";
    }
    if (!listingData.description) {
      missingFields.description = "Description is required.";
    }
    if (!listingData.address) {
      missingFields.address = "Address is required.";
    }
    setErrorMessages(missingFields);

    if (Object.keys(missingFields).length === 0) {
      if (listingData.imageUrls.length < 1) {
        toast.error("You have to upload at least one Image");
      } else if (+listingData.price < +listingData.discountedPrice) {
        toast.error("Discount Price must be lower than Regular Price");
      } else {
        const newPayloadData = { ...listingData, userRef: userInfo._id };
        try {
          setLoading(true);
          const data = await axios.post(
            `/api/list/update/${params.id}`,
            newPayloadData
          );
          if (data.data.success) {
            toast.success(data.data.message);
          }
          setLoading(false);
        } catch (error) {
          if (error.response.data.success === false)
            toast.error(error.response.data.message);
          setLoading(false);
        }
      }
    }
  };

  const fetchListById = async () => {
    try {
      const response = await axios.get(`/api/list/get/${params.id}`);
      const data = response.data.data;
      setListingData(data);
    } catch (error) {
      if (error.response.data.success === false)
        toast.error(error.response.data.message);
    }
  };

  useEffect(() => {
    fetchListById();
  }, [params.id]);

  return (
    <main className="p-3 max-w-4xl mx-auto">
      <h1 className="text-3xl font-semibold text-center my-7">
        Update Listing
      </h1>
      <form
        onSubmit={handleCreateListSubmit}
        className="flex flex-col sm:flex-row gap-4"
      >
        <div className="flex flex-col gap-4 flex-1 ">
          <input
            type="text"
            maxLength="62"
            minLength="10"
            placeholder="Name"
            id="name"
            value={listingData.name}
            onChange={handleInputChange}
            className="border p-3 rounded-lg"
          />
          <span className="text-red-500">{errorMessages.name}</span>
          <textarea
            placeholder="Description"
            id="description"
            value={listingData.description}
            onChange={handleInputChange}
            className="border p-3 rounded-lg"
          />
          <span className="text-red-500">{errorMessages.description}</span>
          <input
            type="text"
            placeholder="Address"
            id="address"
            value={listingData.address}
            onChange={handleInputChange}
            className="border p-3 rounded-lg"
          />
          <span className="text-red-500">{errorMessages.address}</span>

          <div className="flex gap-9 flex-wrap">
            <div className="flex gap-2">
              <input
                type="checkbox"
                name="sale"
                id="sale"
                checked={listingData.type === "sale"}
                onChange={handleInputChange}
              />
              <label>Sale</label>
            </div>
            <div className="flex gap-2">
              <input
                type="checkbox"
                name="rent"
                id="rent"
                checked={listingData.type === "rent"}
                onChange={handleInputChange}
              />
              <label>Rent</label>
            </div>
            {checkTypes.map((check) => (
              <div id={check.id} className="flex gap-2" key={check.id}>
                <input
                  type="checkbox"
                  name={check.label}
                  id={check.id}
                  checked={listingData[check.id]}
                  onChange={handleInputChange}
                />
                <label htmlFor={check.id}>{check.label}</label>
              </div>
            ))}
          </div>
          <div className="flex flex-wrap gap-6">
            <div className="flex items-center gap-2">
              <input
                className="p-3 border border-gray-300 rounded-lg"
                type="number"
                min="1"
                id="bedRooms"
                max="10"
                value={listingData.bedRooms}
                onChange={handleInputChange}
              />
              <p>Beds</p>
            </div>
            <div className="flex items-center gap-2">
              <input
                className="p-3 border border-gray-300 rounded-lg"
                type="number"
                min="1"
                max="10"
                id="bathRooms"
                value={listingData.bathRooms}
                onChange={handleInputChange}
              />
              <p>Baths</p>
            </div>
            <div className="flex items-center gap-2">
              <input
                className="p-3 border border-gray-300 rounded-lg"
                type="number"
                min="1"
                max="1000"
                id="price"
                value={listingData.price}
                onChange={handleInputChange}
              />
              <div className="flex flex-col items-center">
                <p>Price</p>
                <span className="text-xs">($ / month)</span>
              </div>
            </div>
            {listingData?.offer && (
              <div className="flex items-center gap-2">
                <input
                  className="p-3 border border-gray-300 rounded-lg"
                  type="number"
                  min="0"
                  max="100000"
                  id="discountedPrice"
                  value={listingData.discountedPrice}
                  onChange={handleInputChange}
                />
                <div className="flex flex-col items-center">
                  <p>Discounted Price</p>
                  <span className="text-xs">($ / month)</span>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="flex flex-col flex-1 gap-4">
          <p className="font-semibold">
            Images:
            <span className="font-normal text-gray-600 ml-2">
              The first Image will be the cover (max 6 images)
            </span>
          </p>
          <div className="flex gap-4">
            <input
              type="file"
              id="images"
              accept="image/*"
              onChange={(e) => setFiles(e.target.files)}
              className="p-3 border w-full border-gray-300 rounded-lg"
              multiple
            />
            <button
              type="button"
              onClick={handleImageSubmit}
              className="p-3 text-green-700 border border-green-700 rounded hover:shadow-lg disabled:opacity-85 uppercase"
            >
              {uploading ? "Uploading..." : "Upload"}
            </button>
          </div>
          {listingData?.imageUrls?.length > 0 &&
            listingData.imageUrls.map((url) => (
              <div
                key={url}
                className="flex justify-between p-3 border items-center"
              >
                <img
                  src={url}
                  alt="listing-img"
                  className="w-40 h-40 object-cover rounded-lg"
                />
                <button
                  type="button"
                  className="p-3 text-red-700 rounded-lg uppercase hover:opacity-75"
                  onClick={() => handleImageDelete(url)}
                >
                  Delete
                </button>
              </div>
            ))}
          <button
            disabled={loading || uploading}
            className="p-3 bg-slate-700 text-white rounded-lg uppercase hover:opacity-95 disabled:opacity-80"
          >
            {loading ? "Updating.." : "Update List"}
          </button>
        </div>
      </form>
    </main>
  );
}
