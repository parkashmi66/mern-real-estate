import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import toast from "react-hot-toast";
import OAuth from "../components/OAuth";

export default function SignUp() {
  const [signupData, setSignUpData] = useState({
    username: "",
    password: "",
    email: "",
  });

  const [errors, setErrors] = useState({
    username: "",
    password: "",
    email: "",
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const navigate = useNavigate();

  const handleOnchange = (e) => {
    const { id, value } = e.target;
    setSignUpData({ ...signupData, [id]: value });
  };

  const validateForm = () => {
    let isValid = true;
    const newErrors = { ...errors };

    if (!signupData.username) {
      newErrors.username = "Username is required";
      isValid = false;
    } else {
      newErrors.username = "";
    }

    if (!signupData.email) {
      newErrors.email = "Email is required";
      isValid = false;
    } else {
      newErrors.email = "";
    }

    if (!signupData.password) {
      newErrors.password = "Password is required";
      isValid = false;
    } else {
      newErrors.password = "";
    }

    setErrors(newErrors);
    return isValid;
  };
  const postData = async () => {
    try {
      setLoading(true);
      const postedData = await axios.post("/api/auth/signup", signupData);
      if (postedData?.data === false) {
        setError(postedData?.data);
        toast(postedData?.data);
        setLoading(false);
      }
      setLoading(false);
      setError(null);
      navigate("/sign-in");
    } catch (error) {
      console.log("error", error);
    } finally {
      setLoading(false);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Validate the form
    if (validateForm()) {
      postData();
    } else {
      // Form is invalid, do not submit data
      console.log("Form is invalid");
    }
  };

  return (
    <div className="p-3 max-w-lg mx-auto">
      <h1 className="text-3xl text-center font-semibold my-7">Sign Up</h1>
      <form onSubmit={handleSubmit} className="flex flex-col gap-4">
        <input
          type="text"
          placeholder="username"
          className="border p-3 rounded-lg"
          onChange={handleOnchange}
          id="username"
          value={signupData.username}
        />
        {errors.username && <p className="text-red-500">{errors.username}</p>}
        <input
          type="text"
          placeholder="email"
          className="border p-3 rounded-lg"
          onChange={handleOnchange}
          id="email"
          value={signupData.email}
        />
        {errors.email && <p className="text-red-500">{errors.email}</p>}
        <input
          type="password"
          placeholder="password"
          className="border p-3 rounded-lg"
          onChange={handleOnchange}
          id="password"
          value={signupData.password}
        />
        {errors.password && <p className="text-red-500">{errors.password}</p>}
        <button
          disabled={loading}
          className="bg-slate-700 text-white p-3 rounded-lg uppercase hover:opacity-95 disabled:opacity-80"
        >
          {loading ? "Loading..." : "Sign Up"}
        </button>
        <OAuth />
      </form>
      <div className="flex gap-2 mt-5">
        <p>Have an account?</p>
        <Link to={"/sign-in"}>
          <span className="text-blue-700">Sign In</span>
        </Link>
      </div>
    </div>
  );
}
