import React, { useEffect, useRef, useState } from "react";
import userLoggedIn from "../hooks/useAuthenticated";
import {
  getDownloadURL,
  getStorage,
  ref,
  uploadBytesResumable,
} from "firebase/storage";
import { app } from "../../firebase";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteUserFailure,
  deleteUserStart,
  deleteUserSuccess,
  signInStart,
  signOutSuccess,
  updateFailure,
  updateStart,
  updateSuccess,
} from "../redux/user/userSlice";
import toast from "react-hot-toast";
import { Link } from "react-router-dom";

export default function Profile() {
  const userInfo = userLoggedIn();
  const [file, setFile] = useState(null);
  const [uploadPerc, setUploadPerc] = useState(0);
  const [uploadFileError, setUploadFileError] = useState(false);
  const [listingData, setListingData] = useState([]);
  const [formData, setFormData] = useState({
    image: userInfo.avatar,
    username: "",
    password: "",
    email: "",
  });

  const currentPage = 3;
  const SKIP_VALUE = 2;
  const { loading } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const fileRef = useRef();

  const handleImageUpload = (file) => {
    const storage = getStorage(app);
    const fileName = new Date().getTime() + file.name;
    const storageRef = ref(storage, fileName);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setUploadPerc(Math.round(progress));
      },
      (error) => {
        setUploadFileError(true);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) =>
          setFormData({ ...formData, image: downloadUrl })
        );
      }
    );
  };

  const handleOnchange = (e) => {
    const { id, value } = e.target;
    setFormData({ ...formData, [id]: value });
  };

  useEffect(() => {
    if (file) handleImageUpload(file);
  }, [file]);

  const updateUserById = async () => {
    dispatch(updateStart());
    try {
      const data = await axios.post(`/api/users/update/${userInfo._id}`, {
        username: formData.username,
        email: formData?.email,
        avatar: formData?.image,
        password: formData.password,
      });

      if (data.data.success) {
        toast.success(data.data.message);
        dispatch(updateSuccess(data?.data?.data));
      }
    } catch (error) {
      if (error.response.data.success === false)
        toast.error(error.response.data.message);
      dispatch(updateFailure());
    }
  };
  const deleteUserById = async () => {
    dispatch(deleteUserStart());
    try {
      const data = await axios.delete(`/api/users/delete/${userInfo._id}`);
      if (data.data.success) {
        toast.success(data.data.message);
        dispatch(deleteUserSuccess());
      }
    } catch (error) {
      if (error.response.data.success === false)
        toast.error(error.response.data.message);
      dispatch(deleteUserFailure());
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    updateUserById();
  };

  const handleDeleteUser = () => {
    deleteUserById();
  };

  const handleSignOut = async () => {
    try {
      dispatch(signInStart());
      const data = await axios.get(`/api/auth/signout`);
      if (data.data.success) {
        toast.success(data.data.message);
        dispatch(signOutSuccess());
      }
      dispatch(signOutSuccess());
    } catch (error) {
      if (error.response.data.success === false)
        toast.error(error.response.data.message);
    }
  };

  const handleShowListings = async () => {
    try {
      const response = await axios.get(`/api/users/listings/${userInfo._id}`);
      response.data.list
        ? setListingData(response.data.list)
        : setListingData([]);
    } catch (error) {
      if (error.response.data.success === false)
        toast.error(error.response.data.message);
    }
  };

  const handleDeleteList = async (id) => {
    console.log("id is", id);

    try {
      const res = await axios.delete(`/api/list/delete/${id}`);
      if (res?.data?.success) {
        toast.success(res?.data?.message);
      }
    } catch (error) {
      if (error.response.data.success === false)
        toast.error(error.response.data.message);
    } finally {
      handleShowListings();
    }
  };

  return (
    <div className="p-3 max-w-lg mx-auto">
      <h1 className="text-3xl font-semibold text-center my-7">Profile</h1>
      <form
        onSubmit={handleSubmit}
        className="flex flex-col gap-4 justify-center items-center w-full"
      >
        <input
          onChange={(e) => setFile(e.target.files[0])}
          accept="image/*"
          hidden
          type="file"
          ref={fileRef}
        />
        <label htmlFor="fileInput">
          <img
            onClick={() => fileRef.current.click()}
            className="rounded-full h-24 w-24 object-cover cursor-pointer"
            src={formData.image}
            alt="profile"
          />
        </label>
        <p>
          {uploadFileError ? (
            <span className="text-red-700">Error While Uploading Image...</span>
          ) : uploadPerc > 0 && uploadPerc < 100 ? (
            <span className="text-slate-700">{`Uploading ${uploadPerc}%`}</span>
          ) : uploadPerc === 100 ? (
            <span className="text-green-700">
              Image Successfully Uploaded !
            </span>
          ) : null}
        </p>
        <input
          className="border p-3 w-[500px] rounded-lg"
          id="username"
          type="text"
          placeholder="username"
          onChange={handleOnchange}
          defaultValue={userInfo?.username}
        />
        <input
          className="border p-3 w-[500px] rounded-lg"
          id="email"
          type="text"
          placeholder="email"
          onChange={handleOnchange}
          defaultValue={userInfo?.email}
        />
        <input
          className="border p-3 w-[500px] rounded-lg"
          id="password"
          type="password"
          placeholder="password"
          onChange={handleOnchange}
        />
        <button
          disabled={loading}
          className="p-3 w-[500px] rounded-lg opacity-95 disabled:opacity-80 bg-slate-700 text-white uppercase"
          type="submit"
        >
          {loading ? "Loading..." : "  Update"}
        </button>
        <Link
          className=" w-[500px] text-center uppercase hover:opacity-95 bg-green-700 text-white p-3 rounded-lg"
          to={"/create-listing"}
        >
          Create Listing
        </Link>
      </form>
      <div className="flex justify-between text-red-700 mt-5">
        <span onClick={handleDeleteUser} className="cursor-pointer">
          Delete Account
        </span>
        <span onClick={handleSignOut} className="cursor-pointer">
          Sign out
        </span>
      </div>
      <div className="flex justify-center w-full">
        <button
          onClick={handleShowListings}
          type="button"
          className="text-green-700"
        >
          Show Listings
        </button>
      </div>
      {listingData.length > 0 && (
        <div>
          <h1 className="text-center my-7 text-2xl">Your Lists</h1>
          {listingData?.map((lists) => (
            <div
              className="my-4 border rounded-lg p-3 flex gap-4 justify-between items-center"
              key={lists._id}
            >
              <Link to={`/listing/${lists._id}`}>
                <img
                  className="h-16 w-16 object-contain"
                  src={lists.imageUrls[0]}
                  alt={`listing-${lists._id}`}
                />
              </Link>
              <Link
                className="font-semibold text-slate-700 flex-1 hover:underline truncate"
                to={`/listing/${lists._id}`}
              >
                {lists.name}
              </Link>
              <div className="flex flex-col items-center">
                <button
                  onClick={() => handleDeleteList(lists._id)}
                  className="text-red-700 uppercase"
                >
                  Delete
                </button>
                <Link to={`/edit-listing/${lists._id}`}>
                  <button className="text-green-700 uppercase">Edit</button>
                </Link>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
