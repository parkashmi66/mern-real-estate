import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import toast from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";
import {
  signInFailure,
  signInStart,
  signInSuccess,
} from "../redux/user/userSlice";
import OAuth from "../components/OAuth";

export default function SignIn() {
  const [signInData, setSignInData] = useState({
    email: "",
    password: "",
  });

  const [errors, setErrors] = useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { loading } = useSelector((state) => state.user);


  const handleOnChange = (e) => {
    const { id, value } = e.target;
    setSignInData({ ...signInData, [id]: value });
  };

  const validateForm = () => {
    let isValid = true;
    const newErrors = { ...errors };

    if (!signInData.email) {
      newErrors.email = "Email is required";
      isValid = false;
    } else {
      newErrors.email = "";
    }

    if (!signInData.password) {
      newErrors.password = "Password is required";
      isValid = false;
    } else {
      newErrors.password = "";
    }

    setErrors(newErrors);
    return isValid;
  };

  const postData = async () => {
    try {
      dispatch(signInStart());
      const response = await axios.post("/api/auth/signin", signInData);

      if (response?.data?.success === false) {
        dispatch(signInFailure());
        return;
      }
      dispatch(signInSuccess(response?.data?.data));
      navigate("/");
      if (response?.data?.success === true) {
        toast.success(response?.data?.message);
      }
    } catch (error) {
      const errorMessage = error?.response?.data.message;
      toast.error(errorMessage);
      dispatch(signInFailure());
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Validate the form
    if (validateForm()) {
      postData();
    } else {
      // Form is invalid, do not submit data
      console.log("Form is invalid");
    }
  };

  return (
    <div className="p-3 max-w-lg mx-auto">
      <h1 className="text-3xl text-center font-semibold my-7">Sign In</h1>
      <form onSubmit={handleSubmit} className="flex flex-col gap-4">
        <input
          type="text"
          placeholder="email"
          className="border p-3 rounded-lg"
          onChange={handleOnChange}
          id="email"
          value={signInData.email}
        />
        {errors.email && <p className="text-red-500">{errors.email}</p>}
        <input
          type="password"
          placeholder="password"
          className="border p-3 rounded-lg"
          onChange={handleOnChange}
          id="password"
          value={signInData.password}
        />
        {errors.password && <p className="text-red-500">{errors.password}</p>}
        <button
          disabled={loading}
          className="bg-slate-700 text-white p-3 rounded-lg uppercase hover:opacity-95 disabled:opacity-80"
        >
          {loading ? "Loading..." : "Sign In"}
        </button>
        <OAuth/>
      </form>
      <div className="flex gap-2 mt-5">
        <p>Dont have an account?</p>
        <Link to={"/sign-up"}>
          <span className="text-blue-700">Sign Up</span>
        </Link>
      </div>
    </div>
  );
}
