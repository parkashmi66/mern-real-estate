import React, { useEffect, useState } from "react";
import axios from "axios";
import toast from "react-hot-toast";
import { useNavigate, useParams } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore from "swiper";
import { Navigation } from "swiper/modules";
import {
  FaBath,
  FaBed,
  FaChair,
  FaMapMarkedAlt,
  FaMapMarkerAlt,
  FaParking,
  FaShare,
} from "react-icons/fa";
import "swiper/css/bundle";
import userLoggedIn from "../hooks/useAuthenticated";
import Contact from "../components/Contact";

export default function Listing() {
  const [listingData, setListingData] = useState(null);
  console.log("listing Data details0", listingData);
  const [loading, setLoading] = useState(false);
  const [contact, setContact] = useState(false);
  const params = useParams();
  const userInfo = userLoggedIn();

  SwiperCore.use([Navigation]);

  const fetchListById = async () => {
    try {
      setLoading(true);
      const response = await axios.get(`/api/list/get/${params.id}`);
      const data = response?.data?.data;
      setListingData(data);
      setLoading(false);
    } catch (error) {
      if (error.response.data?.success === false)
        toast.error(error.response.data?.message);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchListById();
  }, [params.id]);

  console.log("user redux", userInfo?._id);
  console.log("listing", listingData?.userRef);

  return (
    <div className="">
      {loading && (
        <div className="flex justify-center items-center">Loading.......</div>
      )}
      {!loading && listingData && (
        <>
          <Swiper navigation>
            {listingData?.imageUrls?.map((image, index) => (
              <SwiperSlide key={image}>
                <div
                  className="h-[550px]"
                  style={{
                    background: `url(${image}) center no-repeat`,
                    backgroundSize: "cover",
                  }}
                ></div>
              </SwiperSlide>
            ))}
          </Swiper>
        </>
      )}
      <div className="flex flex-col max-w-4xl mx-auto p-3 my-7 gap-4">
        <p className="text-2xl font-semibold">
          {listingData?.name} - ${" "}
          {listingData?.offer
            ? listingData?.discountedPrice?.toLocaleString("en-US")
            : listingData?.price?.toLocaleString("en-US")}
          {listingData?.type === "rent" && " / month"}
        </p>
        <p className="flex items-center mt-6 gap-2 text-slate-600  text-sm">
          <FaMapMarkerAlt className="text-green-700" />
          {listingData?.address}
        </p>
        <div className="flex gap-4">
          <p className="bg-red-900 w-full max-w-[200px] text-white text-center p-1 rounded-md">
            {listingData?.type === "rent" ? "For Rent" : "For Sale"}
          </p>
          {listingData?.offer && (
            <p className="bg-green-900 w-full max-w-[200px] text-white text-center p-1 rounded-md">
              ${+listingData?.price - +listingData?.discountedPrice} OFF
            </p>
          )}
        </div>
        <p className="text-slate-800">
          <span className="font-semibold text-black">Description - </span>
          {listingData?.description}
        </p>
        <ul className="text-green-900 font-semibold text-sm flex flex-wrap items-center gap-4 sm:gap-6">
          <li className="flex items-center gap-1 whitespace-nowrap ">
            <FaBed className="text-lg" />
            {listingData?.bedRooms > 1
              ? `${listingData?.bedRooms} beds `
              : `${listingData?.bedRooms} bed `}
          </li>
          <li className="flex items-center gap-1 whitespace-nowrap ">
            <FaBath className="text-lg" />
            {listingData?.bathRooms > 1
              ? `${listingData?.bathRooms} baths `
              : `${listingData?.bathRooms} bath `}
          </li>
          <li className="flex items-center gap-1 whitespace-nowrap ">
            <FaParking className="text-lg" />
            {listingData?.parking ? "Parking spot" : "No Parking"}
          </li>
          <li className="flex items-center gap-1 whitespace-nowrap ">
            <FaChair className="text-lg" />
            {listingData?.furnished ? "Furnished" : "Unfurnished"}
          </li>
        </ul>
        {userInfo && listingData?.userRef !== userInfo._id && !contact && (
          <button
            onClick={() => setContact(true)}
            className="bg-slate-700 text-white rounded-lg uppercase hover:opacity-95 p-3"
          >
            Contact landlord
          </button>
        )}
        {contact && <Contact listing={listingData} />}
      </div>
    </div>
  );
}
