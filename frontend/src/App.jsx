import React from "react";
import { Routes, Route, Outlet } from "react-router-dom";
import {
  Homepage,
  About,
  SignIn,
  SignUp,
  Profile,
  CreateListing,
  UpdateListing,
  Listing,
  Search,
} from "./pages";
import { Toaster } from "react-hot-toast";
import Header from "./components/Header";
import useErrorHandlingService from "./interceptors/unauthorized";
import ProctectedRoute from "./components/ProctectedRoute";

function App() {
  useErrorHandlingService();

  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/sign-in" element={<SignIn />} />
        <Route path="/sign-up" element={<SignUp />} />
        <Route path="/search" element={<Search />} />
        <Route path="/listing/:id" element={<Listing />} />
        <Route element={<ProctectedRoute />}>
          <Route path="/profile" element={<Profile />} />
          <Route path="/create-listing" element={<CreateListing />} />
          <Route path="/edit-listing/:id" element={<UpdateListing />} />
        </Route>
        <Route path="/about" element={<About />} />
      </Routes>
      <Outlet />
      <Toaster />
    </>
  );
}

export default App;
