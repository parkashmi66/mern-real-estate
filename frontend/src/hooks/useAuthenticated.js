import { useSelector } from "react-redux";

export default function userLoggedIn() {
  const loginInfo = useSelector((state) => state?.user);
  if (loginInfo) {
    return loginInfo?.userInfo;
  } else {
    return null;
  }
}

//checking
//double-checking